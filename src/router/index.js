import { createRouter, createWebHistory } from "vue-router";
import Home from "../views/Home.vue";
import Hemodisilia from "../views/layanan/Hemodisilia.vue";

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home,
  },
  {
    path: "/hemodisilia",
    name: "Hemodisilia",
    component: Hemodisilia,
  },
  {
    path: "/about",
    name: "About",
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/About.vue"),
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

export default router;
